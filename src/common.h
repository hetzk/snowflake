/*

	Common code.

*/

#ifndef SNOWFLAKE_COMMON_H_
#define SNOWFLAKE_COMMON_H_

#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
	TypeName(const TypeName&); \
	void operator=(const TypeName&)

#endif	// SNOWFLAKE_COMMON_H_